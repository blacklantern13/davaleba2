package com.example.davaleba_2

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.davaleba_2.fragments.DashboardFragment
import com.example.davaleba_2.fragments.InfoFragment
import com.example.davaleba_2.fragments.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private val dashboardFragment = DashboardFragment()
    private val settingsFragment = SettingsFragment()
    private val infoFragment = InfoFragment()
    private lateinit var buttonLogout:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(dashboardFragment)

        var bottomNavigationView: BottomNavigationView = findViewById(R.id.bot_nav)
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.ic_baseline_dashboard_24 -> replaceFragment(dashboardFragment)
                R.id.ic_baseline_settings_24 -> replaceFragment(settingsFragment)
                R.id.ic_baseline_info_24 -> replaceFragment(infoFragment)
            }
            true
        }

        logoutListener()
    }

    private fun replaceFragment(fragment: Fragment){
        if(fragment != null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment)
            transaction.commit()
        }
    }

    private fun logoutListener() {

        val buttonLogout = findViewById<Button>(R.id.buttonLogout)
        buttonLogout.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}